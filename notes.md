running docker container for gosec analyzer locally:

```sh
docker run --volume "$PWD":/code --rm registry.gitlab.com/gitlab-org/security-products/analyzers/gosec:latest /analyzer run --target-dir=/code
```